"""Scrapes Blackboard web content to put it into a sensible model."""

import requests
import urllib.parse
import time, dateutil.parser
import re
from html.parser import HTMLParser
from Model import *

log = open('b.log', 'w')

class BBContentDirectory(ContentDirectory):
    """Represents a folder in Blackboard's content section."""
    def __init__(self, parent, course_id, content_id, palette_id):
        self.parent = parent
        self.course_id = course_id
        self.content_id = content_id
        self.palette_id = palette_id
        self.files = None
    def get_files(self):
        """(Private) Sets the 'self.files' attribute according to the Blackboard contents,
        and returns those files, as a list of objects."""
        if self.files is None:
            self.files = dict(self.parent.get_directory_files(self.course_id, self.content_id))
        return self.files
    def file_names(self):
        """(Overriden from ContentDirectory) Return all files within this folder,
        as a list of strings."""
        return list(self.get_files().keys())
    def get_subobject(self, name):
        """(Overriden from ContentDirectory) Returns the file object with the given
        name, or None if the file doesn't exist."""
        if name in self.get_files():
            return self.get_files()[name]
        else:
            return None
class BBContentFile(ContentFile):
    """Represents a non-folder file in Blackboard's content section."""
    def file(parent, course_id, url):
        """Constructs a subclass of BBContentFile of the appropriate type.
        'course_id' is a string.
        'url' is a string.
        Returns either a BBContentFile, BBAssignment, BBTest, or BBLink."""
        if re.fullmatch('.*/webapps/assignment/.*', url):
            return BBAssignmentFile(parent, course_id, url)
        elif re.fullmatch('.*/webapps/blackboard/content/launchAssessment.*', url):
            return BBTest(parent, course_id, url)
        elif re.fullmatch('.*webdav/pid.*/xid.*', url):
            return BBContentFile(parent, course_id, url)
        else:
            return BBLink(url)
    def __init__(self, parent, course_id, url):
        """Constructs a new BBContentFile.
        Use BBContentFile.file(), as this constructor should not be called directly."""
        self.parent = parent
        self.course_id = course_id
        url_match = re.fullmatch('.*/pid-([_0-9]+)-.*/xid-([_0-9]+).*', url)
        self.content_id = '_' + url_match.group(1) + '_1'
        self.xid = url_match.group(2)
        self.time = None
        self.data = None
    def get_size(self):
        """(Overridden from ContentFile) Returns the number of bytes in the file.
        If the file contents have not been downloaded yet, it will return 0."""
        if self.data is None:
            return 0
        else:
            return len(self.data)
    def get_data(self):
        """(Overridden from ContentFile) Returns the contents of the file, as a list of bytes."""
        if self.data is None:
            self.data = self.parent.get_file_contents(self.course_id, self.content_id, self.xid)
        return self.data
    def get_time(self):
        """(Overriden from ContentFile) Returns the Unix time associated with the file."""
        if self.time is None:
            self.time = self.parent.get_file_metadata(self.course_id, self.content_id)
        return self.time
class BBTextFile(ContentFile):
    def __init__(self, parent, contents):
        self.contents = contents.encode('utf-8')
    def get_data(self):
        return self.contents
class BBVideoFile(BBContentFile):
    def __repr__(self):
        return super().__repr__() + '[V]'
class BBAssignmentFile(ContentFile):
    def __init__(self, parent, course_id, url):
        pass
    def __repr__(self):
        return super().__repr__() + '[A]'
class BBTest(ContentFile):
    def __init__(self, parent, course_id, url):
        pass
    def __repr__(self):
        return super().__repr__() + '[T]'
class BBLink(ContentFile):
    def __init__(self, url):
        self.url = url

class BBUser(User):
    def __init__(self, student_id, last_name, first_name, email, role, course):
        super().__init__(student_id, last_name, first_name, email, role)
        self.course = course
        self.gradesheet_id = None
    def get_gradesheet_id(self):
        if self.gradesheet_id is None:
            self.gradesheet_id = self.course.get_gradesheet_id_for_user(self)
        return self.gradesheet_id

class BBCourse(Course):
    def __init__(self, parent, course_id, name):
        super().__init__(course_id, name)
        self.bb = parent
        self.content_sections = None
        self.user_list = None
        self.grades = None
    def get_content_sections(self):
        if self.content_sections is None:
            self.content_sections = self.bb.get_content_sections(self.id)
        return self.content_sections
    def get_users(self):
        if self.user_list is None:
            self.user_list = self.bb.get_user_list(self)
        return self.user_list
    def get_raw_gradesheet(self):
        """(Private) Returns the JSON object returned by Blackboard."""
        self.bb.navigate_to_course(self.id)
        params = { 'course_id': self.id, 'flush': 'true' }
        json = self.bb.get_relative('webapps/gradebook/do/instructor/getJSONData', params).json()
        return json
    def get_grades(self):
        """Returns a list of Grade objects."""
        if self.grades is None:
            json = self.get_raw_gradesheet()
            def make_grade(grade_type, name, time, due):
                if grade_type == 'resource/x-bb-assignment':
                    return BBAssignment(self, name, time, due)
                elif grade_type == 'resource/x-bb-assessment':
                    #return Quiz(self, name, time, due)
                    return BBAssignment(self, name, time, due)
            self.grades = [make_grade(x['src'], x['name'], int(x['cdate']) // 1000, int(x['due']) // 1000)
                for x in json['colDefs'] if 'src' in x]
            self.grades = [g for g in self.grades if g is not None]
        return self.grades
    def create_content_section(self, name):
        self.bb.add_content_section(self.id, name)
        self.content_sections = None
    def destroy_content_section(self, name):
        section_id = [s.palette_id for nm, s in self.get_content_sections() if nm == name]
        if len(section_id) != 1:
            return
        section_id = section_id[0]
        self.bb.remove_content_section(self.id, section_id)
        self.content_sections = None
    def get_gradesheet_id_for_user(self, user):
        student_uid = user.student_id
        gradesheet = self.get_raw_gradesheet()
        for student in gradesheet['rows']:
            if student[2]['v'].strip() == student_uid:   # FIXME: too brittle?
                return student[0]['uid'].strip()
    def get_assignment_id(self, assignment):
        gradesheet = self.get_raw_gradesheet()
        for a in gradesheet['colDefs']:
            if a['name'].strip() == assignment.name:
                return a['id'].strip()
class BBAssignment(Assignment):
    def __init__(self, course, name, time, due):
        super().__init__(course, name, time, due)
    def make_submission(self, student):
        return BBAssignmentSubmissions(self.course.bb, self, student)
    def get_assignment_id(self):
        return self.course.get_assignment_id(self)
class BBAssignmentSubmissions(GradeSubmissions):
    def __init__(self, blackboard, assignment, student):
        super().__init__()
        self.blackboard = blackboard
        self.assignment = assignment
        self.student = student
        self.submissions = None
    def get_submissions(self):
        if self.submissions is None:
            assign = self.assignment
            course = assign.course
            self.submissions = self.get_student_submissions(course.id,
                assign.get_assignment_id(), self.student.get_gradesheet_id())
        return self.submissions
    def get_student_submissions(self, course_id, assignment_id, student_uid):
        """Returns information about different submissions to an assignment or quiz.
        'course_id' is a string.
        'assignment_id' is a string taken from the course's gradebook.
        'student_uid' is NOT the student ID, but rather the student 'uid', taken from the course's gradebook.
        Returns a list of StudentSubmission objects."""
        self.blackboard.navigate_to_course(course_id)
        params = { 'course_id': course_id, 'outcomeDefinitionId': assignment_id,
            'courseMembershipId': student_uid }
        response = self.blackboard.get_relative('webapps/gradebook/do/instructor/viewGradeDetails', params)
        return StudentSubmissionScraper(self).get_submission_details(response.text)
class BBStudentSubmission(StudentSubmission):
    def __init__(self, blackboard, parent, params_for_update, submission_id, date, grade, feedback):
        self.blackboard = blackboard
        self.parent = parent
        self.params_for_update = params_for_update
        super().__init__(submission_id, date, grade, feedback)
        self.cached_submissions = None
    def get_files(self):
        if self.cached_submissions is None:
            course_id = self.parent.assignment.course.id
            assignment_id = self.parent.assignment.get_assignment_id()
            submission_id = self.id
            student_uid = self.parent.student.get_gradesheet_id()
            self.cached_submissions = self.blackboard.get_student_submission_files(course_id,
                assignment_id, submission_id, student_uid)
        return self.cached_submissions
    def file_is_finished(self, filename, data):
        super().file_is_finished(filename, data)
        if filename == 'grade' or filename == 'feedback' and self.grade and self.feedback:
            assignment_id = self.parent.assignment.get_assignment_id()
            course_id = self.parent.assignment.course.id
            attempt_id = self.id
            student_uid = self.parent.assignment.course.get_gradesheet_id_for_user(self.parent.student)
            a = self.blackboard.give_student_feedback(self.params_for_update, course_id, assignment_id, attempt_id, student_uid, self.grade, self.feedback)

class BBDownloadFile(ContentFile):
    def __init__(self, blackboard, course_id, attempt_id, file_id, filename):
        super().__init__()
        self.blackboard = blackboard
        self.course_id = course_id
        self.attempt_id = attempt_id
        self.file_id = file_id
        self.filename = filename
        self.data = None
    def get_data(self):
        if self.data is None:
            self.blackboard.navigate_to_course(self.course_id)
            params = { 'course_id': self.course_id, 'attempt_id': self.attempt_id,
                'file_id': self.file_id, 'fileName': self.filename }
            self.data = self.blackboard.get_relative('webapps/assignment/download', params).content
        return self.data

def blackboard_time_to_unix_time(blackboard):
    """'Blackboard time' is general human-readable time, such as 'Mar 20, 2019 11:35 PM'.
    Returns an int, representing this time in Unix time.
    Since Blackboard seems to typically run in localtime, no timezone conversion is done."""
    return int(time.mktime(dateutil.parser.parse(blackboard).timetuple()))

class HTMLTag:
    """A simple class used to compare HTML elements.
    An HTMLTag consists of the tag itself (e.g., 'a') and a dictionary of attributes."""
    def __init__(self, tag, attrs):
        self.tag = tag
        self.attrs = dict(attrs)
    def __le__(self, other):
        """One tag is <= another tag if they have the same tag and a subset of attributes."""
        if self.tag != other.tag:
            return False
        for attr_name in self.attrs:
            if not (attr_name in other.attrs and self.attrs[attr_name] == other.attrs[attr_name]):
                return False
        return True
    def __repr__(self):
        return f'<{self.tag} {self.attrs}>'
class HTMLRepeatTag:
    """An HTMLRepeatTag is used to match optional repetitions of a tag."""
    def __init__(self, tag):
        self.tag = tag
    def __le__(self, other):
        return self.tag <= other
class HTMLHierarchyScraper(HTMLParser):
    """A convenienc class to make certain patterns of HTML scraping easier to do.
    Any subclass will override handle_data.
    They can then use matches_recent_tags to determine if they are in the correct part of
    the HTML syntax tree."""
    def __init__(self):
        super().__init__()
        self.tags = []
    def handle_starttag(self, tag, attrs):
        """(Overriden from HTMLParser) Adds the current tag to the stack."""
        self.tags.append(HTMLTag(tag, attrs))
    def handle_endtag(self, tag):
        """(Overridden from HTMLParser) Removes the current tag from the stack."""
        for i in range(len(self.tags) - 1, -1, -1):
            if self.tags[i].tag == tag:
                self.tags = self.tags[:i]
                return
    def matches_recent_tags(self, tags):
        """Determines if we are in the correct part of the HTML syntax tree.
        'tags' is a list, where each element is either an HTMLTag or HTMLRepeatTag.
        If the end of the current stack of tags matches (via the <= operator) the
        list of tags or repeat-tags given, then we return True.
        Otherwise, we return False."""
        if len(self.tags) < len(tags):
            return False
        match_i = -1
        for i in range(len(tags)):
            if -match_i > len(tags):
                break
            while not (tags[match_i] <= self.tags[-(i + 1)]) \
            and -match_i <= len(tags) and isinstance(tags[match_i], HTMLRepeatTag):
                match_i -= 1
            if tags[match_i] <= self.tags[-(i + 1)]:
                if not isinstance(tags[match_i], HTMLRepeatTag):
                    match_i -= 1
            else:
                return False
        return True
class ContentSectionScraper(HTMLHierarchyScraper):
    """Scrapes a Blackboard course home page to determine what the major content sections are.
    Note that top-level content sections are different in Blackboard compared to other folders."""
    def __init__(self, parent, course_id):
        super().__init__()
        self.parent = parent
        self.course_id = course_id
        self.sections = []
    def get_sections(self, html):
        """Gets all top-level content sections.
        'html' is a string with HTML text from a course home page.
        Returns a list of BBContentFile or BBContentDirectory objects."""
        self.sections = []
        self.feed(html)
        return self.sections
    def handle_data(self, data):
        """(Overridden from HTMLHierarchyScraper) Adds new sections to the self.sections list."""
        if self.matches_recent_tags([HTMLTag('li', {}), HTMLTag('a', {}), HTMLTag('span', {})]) and \
        'id' in self.tags[-3].attrs:
            a_href = self.tags[-2].attrs['href']
            li_id = re.fullmatch('paletteItem:*([_0-9]+)', self.tags[-3].attrs['id'])
            href_match = re.fullmatch('.*/listContentEditable.jsp.*[?&;]content_id=([_0-9]+).*', a_href)
            if href_match and li_id:
                directory = BBContentDirectory(self.parent, self.course_id, href_match.group(1),
                    li_id.group(1))
                self.sections.append((data, directory))
                return
            href_match = re.fullmatch('.*/contentWrapper.jsp.*[?&;]href=([^&;]+).*', a_href)
            if href_match:
                new_file = BBContentFile.file(self.parent, self.course_id,
                    urllib.parse.unquote(href_match.group(1)))
                self.sections.append((data, new_file))
                return
class DirectoryScraper(HTMLHierarchyScraper):
    """Scrapes a directory that's present in a Blackboard content section."""
    def __init__(self, parent, course_id):
        super().__init__()
        self.parent = parent
        self.course_id = course_id
        self.files = []
        self.to_be_completed = None
        self.built_text = None
    def get_files(self, html):
        """Gets all files within this directory.
        Returns a list of objects."""
        self.files = []
        self.to_be_completed = None
        self.built_text = None
        self.feed(html)
        return self.files
    def handle_data(self, data):
        """(Overridden from HTMLHierarchyScraper) Adds new content to the list."""
        base_tags = [HTMLTag('ul', { 'class': 'contentList' }),
            HTMLTag('li', { 'class': 'clearfix liItem' }), HTMLTag('div', {}), HTMLTag('h3', {})]
        if self.matches_recent_tags(base_tags + [HTMLTag('a', {}), HTMLTag('span', {})]):
            new_file = BBContentFile.file(self.parent, self.course_id, self.tags[-2].attrs['href'])
            self.files.append((data, new_file))
            self.to_be_completed = None
            self.built_text = None
        elif self.matches_recent_tags(base_tags + [HTMLTag('span', {})]) and len(data.strip()) > 0:
            self.to_be_completed = data
        elif self.matches_recent_tags([HTMLTag('param', { 'name': 'Url' })]) and \
        self.to_be_completed is not None:
            new_file = BBVideoFile(self.parent, self.course_id, self.tags[-1].attrs['value'])
            self.files.append((self.to_be_completed, new_file))
            self.to_be_completed = None
            self.built_text = None
        elif self.matches_recent_tags([HTMLTag('div', { 'class': 'vtbegenerated_div' }),
        HTMLRepeatTag(HTMLTag('br', {})), HTMLTag('span', {})]):
            if self.built_text is None:
                self.built_text = ''
            if self.tags[-2].tag == 'div':
                self.built_text += '\n'
            self.built_text += data + '\n'
        elif self.matches_recent_tags([HTMLTag('div', { 'class': 'moduleSample' })]) and \
        self.built_text is not None and self.to_be_completed is not None:
            new_file = BBTextFile(self.parent, self.built_text.strip())
            self.files.append((self.to_be_completed, new_file))
            self.to_be_completed = None
            self.built_text = None
class MetadataScraper(HTMLHierarchyScraper):
    """Scrapes metadata from a file, by going to the file's "metadata" page."""
    def __init__(self):
        super().__init__()
        self.last_label = []
        self.time = None
    def get_metadata(self, html):
        """Get all metadata for this file (right now that's not very much).
        'html' is a string.
        Returns an integer, representing the time."""
        self.last_label = ''
        self.feed(html)
        return self.time
    def handle_data(self, data):
        """(Overridden from HTMLHierarchyScraper) Update scraping state."""
        if self.matches_recent_tags([HTMLTag('ol', {}), HTMLTag('li', {}),
        HTMLTag('div', { 'class': 'label' })]):
            self.last_label = data.strip()
        elif self.matches_recent_tags([HTMLTag('ol', {}), HTMLTag('li', {}),
        HTMLTag('div', { 'class': 'field' })]):
            if self.last_label == 'Creation Date':
                self.time = blackboard_time_to_unix_time(data)
                self.last_label = ''
class StudentSubmissionScraper(HTMLHierarchyScraper):
    def __init__(self, student_submissions):
        super().__init__()
        self.student_submissions = student_submissions
        self.submissions = []
        self.clear_values()
    def clear_values(self):
        self.date = None
        self.grade = None
        self.feedback = None
        self.id = None
        self.edit_params = {}
    def get_submission_details(self, html):
        self.submissions = []
        self.feed(html)
        return self.submissions
    def handle_data(self, data):
        if self.matches_recent_tags([HTMLTag('td', { 'headers': 'creation_date_col' }), HTMLTag('div', {})]):
            self.date = blackboard_time_to_unix_time(data)
        elif self.matches_recent_tags([HTMLTag('td', { 'headers': 'value_col' }), HTMLTag('div', {}),
        HTMLTag('span', {})]):
            try:
                self.grade = float(data)
            except:
                self.grade = None
        elif self.matches_recent_tags([HTMLTag('td', { 'class': 'bWhite' }), HTMLTag('textarea', {})]):
            self.feedback = data.strip()
        elif self.matches_recent_tags([HTMLTag('td', { 'headers': 'actions_col' }),
        HTMLTag('div', {}), HTMLTag('a', {})]) and data.strip() == 'View Attempt':
            onclick_code = self.tags[-1].attrs['onclick']
            first_apostrophe = onclick_code.index("'")
            second_apostrophe = onclick_code.index("'", first_apostrophe + 1)
            self.id = onclick_code[first_apostrophe + 1:second_apostrophe]
        elif self.matches_recent_tags([HTMLTag('td', { 'headers': 'actions_col' }),
        HTMLTag('div', { 'class': 'editgrade' }), HTMLTag('fieldset', {}), HTMLTag('div', {}),
        HTMLTag('ol', {}), HTMLTag('li', {}), HTMLTag('div', {}),
        HTMLTag('p', { 'class': 'taskbuttondiv' })]):
            ss = self.student_submissions
            self.submissions.insert(0, BBStudentSubmission(ss.blackboard, ss, self.edit_params,
                self.id, self.date, self.grade, self.feedback))
            self.clear_values()
    def handle_starttag(self, tag, attrs):
        if self.matches_recent_tags([HTMLTag('form', { 'id': 'modifyGradeForm' })]) \
        and tag == 'input':
            a = dict(attrs)
            self.edit_params[a['name']] = a['value']
        super().handle_starttag(tag, attrs)
    def handle_startendtag(self, tag, attrs):
        attrs = dict(attrs)
        if self.matches_recent_tags([HTMLTag('td', { 'headers': 'actions_col' }),
        HTMLTag('div', { 'class': 'editgrade' }), HTMLTag('fieldset', {}), HTMLTag('div', {}),
        HTMLTag('ol', {}), HTMLTag('li', {}), HTMLTag('div', { 'class': 'editBlock' })]) \
        and tag == 'input' and attrs['type'] == 'hidden':
            param_key = re.sub(r'[0-9]', '', attrs['id'])
            param_value = attrs['value']
            self.edit_params[param_key] = param_value
class StudentSubmissionFilesScraper(HTMLHierarchyScraper):
    def __init__(self, blackboard, course_id, attempt_id):
        super().__init__()
        self.files = {}
        self.blackboard = blackboard
        self.course_id = course_id
        self.attempt_id = attempt_id
    def get_files(self, html):
        self.files = {}
        self.feed(html)
        return self.files
    def get_file(self, data, tag):
        filename = data.strip()
        if filename != 'Submission Text':
            file_id = tag.attrs['id']
            file_id = file_id[file_id.index('_', file_id.index('_') + 1):]
            return filename, BBDownloadFile(self.blackboard, self.course_id, self.attempt_id, file_id, filename)
        else:
            return filename, DirectContentFile('Submission text is not supported yet :(')
    def handle_data(self, data):
        if self.matches_recent_tags([HTMLTag('ul', { 'class': 'filesList' }), HTMLTag('li', {}),
        HTMLTag('a', {}), HTMLTag('span', {})]):
            filename, file = self.get_file(data, self.tags[-2])
            self.files[filename] = file
        elif self.matches_recent_tags([HTMLTag('ul', { 'class': 'filesList' }), HTMLTag('li', {}),
        HTMLTag('a', {})]) and data.strip() != '':
            filename, file = self.get_file(data, self.tags[-1])
            self.files[filename] = file

class UserListScraper(HTMLHierarchyScraper):
    """Scrapes the list of all users for a course."""
    def __init__(self, course):
        super().__init__()
        self.users = []
        self.cur_user = []
        self.course = course
    def get_users(self, html):
        """Returns all users for this course.
        'html' is a string.
        Returns a list of BBUser objects."""
        self.users = []
        self.cur_user = []
        self.feed(html)
        return self.users
    def handle_data(self, data):
        """(Overridden from HTMLHierarchyScraper) Updates scraping state."""
        if self.matches_recent_tags([HTMLTag('span', { 'class': 'profileCardAvatarThumb' })]) \
        and len(data.strip()) > 0:
            self.cur_user = [data.strip()]
        elif self.matches_recent_tags([HTMLTag('td', { 'class': '', 'valign': 'top' }),
        HTMLTag('span', { 'class': 'table-data-cell-value' }), HTMLTag('a', {})]):
            self.cur_user.append(data.strip())
        elif self.matches_recent_tags([HTMLTag('td', { 'class': '', 'valign': 'top' }),
        HTMLTag('span', { 'class': 'table-data-cell-value' })]):
            self.cur_user.append(data.strip())
            if len(self.cur_user) == 7:
                new_user = BBUser(self.cur_user[0], self.cur_user[2], self.cur_user[1], self.cur_user[3],
                    self.cur_user[5], self.course)
                self.users.append(new_user)
                self.cur_user = []
class TagScraper(HTMLParser):
    """An abstract type of scraper that looks for tags, regardless of context."""
    def __init__(self, html):
        super().__init__()
        self.html = html
    def get_tag_values(self, search_tag, search_attr_name, search_attr_value, target_attr_name):
        """Get all values that match a particular tag, regardless of context in the HTML syntax tree.
        'search_tag' is a string, such as 'input'.
        'search_attr_name' is a string, such as 'name'
        'search_attr_value' is a string, such as 'password'
        'target_attr_name' is a string, such as 'value'.
        Returns a string, for the value that matches the target attribute name."""
        self.search_tag = search_tag
        self.search_attr_name = search_attr_name
        self.search_attr_value = search_attr_value
        self.target_attr_name = target_attr_name
        self.target_attr_value = None
        self.vals = []
        self.get_data = False
        self.feed(self.html)
        return self.vals
    def get_form_value(self, name):
        """Gets the first value for an input with the given name that's found.
        'name' is a string.
        Returns a string."""
        attr_value, _ = self.get_tag_values('input', 'name', name, 'value')[0]
        return attr_value
    def handle_starttag(self, tag, attrs):
        """(Overridden from HTMLParser)"""
        attrs = dict(attrs)
        if tag == self.search_tag and self.search_attr_name in attrs and \
        attrs[self.search_attr_name] == self.search_attr_value:
            self.target_attr_value = attrs[self.target_attr_name]
            self.get_data = True
    def handle_data(self, data):
        """(Overridden from HTMLParser) Updates the scraping state."""
        if self.get_data:
            self.vals.append((self.target_attr_value, data))
            self.get_data = False
class CourseListScraper(TagScraper):
    """Gets a list of all Blackboard courses, from the Blackboard first page."""
    def __init__(self, parent, html):
        # The first 2 lines of HTML are XML nonsense. Ignore them
        self.parent = parent
        html = html[html.index('\n', html.index('CDATA') + 1):]
        super().__init__(html)
    def get_courses(self):
        """Returns a list of Course objects."""
        return [BBCourse(self.parent, re.fullmatch('/.*&.*id=([_0-9]+).*', url.strip()).group(1), name)
            for url, name in self.get_tag_values('a', 'target', '_top', 'href')]
class Blackboard:
    """Handles one connection/session with Blackboard."""
    def __init__(self, base_url, user):
        self.base_url = base_url
        self.user = user
        self.courses = None
        self.current_course = None
        self.current_course_text = ''
        self.nonce = None
    def get(self, url, params=None):
        """Does an HTTPS GET and updates cookies."""
        if params is None:
            params = {}
        response = requests.get(url, params=params, cookies=self.cookies)
        self.cookies.update(response.cookies)
        return response
    def get_relative(self, url, params=None):
        return self.get(f'{self.base_url}/{url}', params)
    def post(self, url, data, params=None):
        """Does an HTTPS POST and updates cookies."""
        if params is None:
            params = {}
        response = requests.post(url, params=params, data=data, cookies=self.cookies)
        self.cookies.update(response.cookies)
        return response
    def login(self):
        """Logs in."""
        response = requests.get(self.base_url)
        self.cookies = response.cookies
        response = TagScraper(response.text)
        self.nonce = response.get_form_value('blackboard.platform.security.NonceUtil.nonce.ajax')
        data = { 'user_id': self.user['username'], 'password': self.user['password'], 'login': 'Login',
            'action': 'login', 'new_loc': '', 'blackboard.platform.security.NonceUtil.nonce.ajax': self.nonce }
        self.post(f'{self.base_url}/webapps/login/', data)
    def get_course_list(self):
        """Returns a list of Course objects."""
        if self.courses is None:
            data = { 'action': 'refreshAjaxModule', 'modId': '_22_1', 'tabId': '_2_1',
                'tab_tab_group_id': '_2_1' }
            response = self.post(f'{self.base_url}/webapps/portal/execute/tabs/tabAction', data)
            course_scraper = CourseListScraper(self, response.text)
            self.courses = course_scraper.get_courses()
        return self.courses
    def get_nonce_from_course(self, course_id):
        """Every course has a nonce value that must be used with all API calls.
        This function returns the nonce for a particular course.
        'course_id' is a string.
        Returns a string."""
        response = self.navigate_to_course(course_id)
        return TagScraper(response.text).get_form_value('blackboard.platform.security.NonceUtil.nonce.ajax')
    def navigate_to_course(self, course_id):
        """Simulates navigating to the course, so that the bbRouter cookie is updated."""
        if course_id != self.current_course:
            self.current_course_text = self.get(f'{self.base_url}/webapps/blackboard/execute/launcher',
                { 'type': 'Course', 'id': course_id, 'url': '' })
            self.current_course = course_id
        return self.current_course_text
    def get_user_list(self, course):
        """'course' is a Course object.
        Returns a list of BBUser objects for a particular course."""
        self.navigate_to_course(course.id)
        data = { 'course_id': course.id, 'editPaging': 'true', 'numResults': '999' }
        response = self.get(f'{self.base_url}/webapps/blackboard/execute/userManager', data)
        return UserListScraper(course).get_users(response.text)
    def get_content_sections(self, course_id):
        """Returns a list of BBContentDirectory and BBContentFile objects for a particular course."""
        self.navigate_to_course(course_id)
        params = { 'course_id': course_id, 'task': 'true', 'src': '' }
        response = self.get(f'{self.base_url}/webapps/blackboard/execute/courseMain', params)
        return ContentSectionScraper(self, course_id).get_sections(response.text)
    def get_directory_files(self, course_id, content_id):
        """Returns files/directories within a given directory in the content section
        of a Blackboard course."""
        self.navigate_to_course(course_id)
        params = { 'course_id': course_id, 'content_id': content_id, 'mode': 'reset' }
        response = self.get(f'{self.base_url}/webapps/blackboard/content/listContentEditable.jsp', params)
        return DirectoryScraper(self, course_id).get_files(response.text)
    def get_file_metadata(self, course_id, content_id):
        """Returns the Unix time of a particular content item."""
        self.navigate_to_course(course_id)
        params = { 'course_id': course_id, 'content_id': content_id, 'cmd': 'view' }
        response = self.get(f'{self.base_url}/webapps/blackboard/execute/manageMetadata', params)
        return MetadataScraper().get_metadata(response.text)
    def add_content_section(self, course_id, section_name, visible=True):
        """Creates a new top-level directory.
        'course_id' is a string.
        'section_name' is a user-defined string.
        'visible' is a boolean indicating whether students should be able to see this section."""
        nonce = self.get_nonce_from_course(course_id)
        if visible:
            visible = 'Y'
        else:
            visible = 'N'
        data = { 'blackboard.platform.security.NonceUtil.nonce.ajax': nonce, 'course_id': course_id,
            'targetType': 'CONTENT', 'name': section_name, 'enabled': visible }
        self.post(f'{self.base_url}/webapps/blackboard/execute/course/addtoc', data)
        self.courses = None # flush the cache
    def remove_content_section(self, course_id, section_id):
        """Deletes a top-level directory.
        'course_id' is a string.
        'section_id' is a string."""
        nonce = self.get_nonce_from_course(course_id)
        params = { 'cmd': 'removeToc', 'course_id': course_id, 'toc_id': section_id,
            'blackboard.platform.security.NonceUtil.nonce.ajax': nonce,
            'retUrl': f'{self.base_url}/webapps/blackboard/execute/modulepage/view?course_id={course_id}' }
        self.post(f'{self.base_url}/webapps/blackboard/execute/doCourseMenuAction', {}, params)
        self.courses = None # flush the cache
    def get_student_submission_files(self, course_id, assignment_id, attempt_id, student_uid):
        self.navigate_to_course(course_id)
        params = { 'outcomeDefinitionId': f'_{assignment_id}_1', 'sequenceId': course_id,
            'course_id': course_id, 'attempt_id': attempt_id, 'courseMembershipId': f'_{student_uid}_1',
            'anonymousMode': 'false', 'singleAttempt': 'true' }
        response = self.get(f'{self.base_url}/webapps/assignment/gradeAssignmentRedirector', params)
        return StudentSubmissionFilesScraper(self, course_id, attempt_id).get_files(response.text)
    def give_student_feedback(self, params, course_id, assignment_id, attempt_id, student_uid, grade, feedback):
        params['course_id'] = course_id
        params['outcomeDefinitionId'] = f'_{assignment_id}_1'
        params['courseMembershipId'] = f'_{student_uid}_1'
        params['attemptId'] = attempt_id
        params['score'] = grade
        params['grade'] = grade
        params['instructorNotes'] = ''
        params['feedBackToUser'] = ''
        params['exemptChanged'] = 'false'
        params['instructorNotesChanged'] = 'false'
        params['feedBackToUserChanged'] = 'true'
        if 'pre__rubricEvaluation' in params:
            params['_rubricEvaluation'] = params['pre__rubricEvaluation']
        params['feedBackToUsertext'] = feedback
        r = self.post(f'{self.base_url}/webapps/gradebook/do/instructor/modifyGrade2', params)
