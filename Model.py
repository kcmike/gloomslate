"""Defines an abstract data model to bridge between Blackboard and FUSE."""

from datetime import datetime
import toml

log = open('m.log', 'w')

class GeneralFile:
    """Any node in the filesystem tree."""
    def can_write(self):
        """True if the file/directory is writeable."""
        return False
    def get_time(self):
        """A Unix time integer."""
        return 0
    def flush_data(self):
        pass

class ContentDirectory(GeneralFile):
    """A directory in the filesystem tree."""
    def is_directory(self):
        """This should not be overridden by any subclass."""
        return True
    def allow_create(self):
        return False
    def file_names(self):
        """A list of files (strings) in this directory."""
        return []
class DictionaryBasedDirectory(ContentDirectory):
    """A simple directory with fixed contents."""
    def __init__(self, dictionary):
        self.dictionary = dictionary
    def file_names(self):
        return list(self.dictionary.keys())
    def get_subobject(self, filename):
        """Returns an object given a filename."""
        if filename in self.dictionary:
            return self.dictionary[filename]
        else:
            return None

class ContentFile(GeneralFile):
    """A regular file (non-directory) in the filesystem tree."""
    def is_directory(self):
        """This should not be overridden by any subclass."""
        return False
    def to_dict(self):
        """Used to produce content, if get_data is not overridden."""
        return {}
    def get_size(self):
        """Size of contents, in number of bytes."""
        return len(self.get_data())
    def get_data(self):
        """Contents, as a list of bytes."""
        return toml.dumps(self.to_dict()).encode('utf-8')
class DirectContentFile(ContentFile):
    def __init__(self, contents):
        self.contents = (contents.strip() + '\n').encode('utf-8')
    def get_data(self):
        return self.contents

class ExtensibleContentDirectory(ContentDirectory):
    class UnfinishedFile(ContentFile):
        def __init__(self, parent, filename):
            super().__init__()
            self.parent = parent
            self.bytes = b''
            self.filename = filename
        def __repr__(self):
            return f'Unfinished file {self.filename} with bytes {self.bytes}'
        def can_write(self):
            return True
        def write_bytes(self, data, offset):
            log.write(f'write_bytes({data}, {offset})\n')
            log.flush()
            if len(self.bytes) > offset:
                self.bytes = self.bytes[:offset]
            elif len(self.bytes) < offset:
                self.bytes += b'\x00' * (offset - len(self.bytes))
            self.bytes += data
        def flush_data(self):
            log.write('flush_data\n')
            log.flush()
            if len(self.bytes) > 0:
                self.parent.file_is_finished(self.filename, self.bytes)
        def get_data(self):
            return self.bytes
    def __init__(self):
        super().__init__()
        self.unfinished_files = {}
    def can_write(self):
        return True
    def create_file(self, path):
        log.write(f'line 93: create_file({path})\n')
        log.flush()
        self.unfinished_files[path] = ExtensibleContentDirectory.UnfinishedFile(self, path)
        return True
    def get_subobject(self, path):
        log.write(f'line 98 get_subobject: {path} of {self.unfinished_files}\n')
        log.flush()
        if path in self.unfinished_files:
            return self.unfinished_files[path]
        else:
            return None

class User(ContentFile):
    """A user, including student, instructor, or TA."""
    def __init__(self, student_id, last_name, first_name, email, role):
        self.student_id = student_id
        self.last_name = last_name
        self.first_name = first_name
        self.email = email
        self.role = role
    def to_dict(self):
        return { 'student_id': self.student_id, 'last_name': self.last_name, 'first_name': self.first_name,
            'email': self.email, 'role': self.role }
    def from_dict(dictionary):
        """Deserializes a dictionary back into a User object."""
        return User(dictionary['student_id'], dictionary['last_name'], dictionary['first_name'],
            dictionary['email'], dictionary['role'])

class Grade(ContentDirectory):
    """A grade column, such as 'Assignment 01' or 'Quiz 02'."""
    def __init__(self, course, name, time, due=None):
        self.course = course
        self.name = name
        self.time = time
        self.due = due
        self.submissions = None
    def get_time(self):
        return self.time
    def file_names(self):
        yield 'info'
        yield 'submissions'
    def get_subobject(self, filename):
        if filename == 'info':
            return GradeInfoFile(self)
        elif filename == 'submissions':
            if self.submissions is None:
                self.submissions = DictionaryBasedDirectory({ x.student_id: self.make_submission(x)
                    for x in self.course.get_users() })
            return self.submissions
        return None
class GradeInfoFile(ContentFile):
    def __init__(self, grade):
        super().__init__()
        self.grade = grade
    def to_dict(self):
        return { 'due_date': datetime.fromtimestamp(self.grade.due) }
class GradeSubmissions(ContentDirectory):
    def __init__(self):
        super().__init__()
    def get_submissions(self):
        return []
    def file_names(self):
        return ['%02d' % i for i in range(len(self.get_submissions()))]
    def make_submission(self, student):
        """(Protected: called by get_subobject)"""
        return None
    def get_subobject(self, num):
        try:
            return self.get_submissions()[int(num)]
        except Exception:
            return None
class Assignment(Grade):
    def __init__(self, course, name, time, due=None):
        super().__init__(course, name, time, due)
        self.attachments = {}
        self.submissions = None
    def file_names(self):
        yield from super().file_names()
        for k in self.attachments:
            yield k
    def get_subobject(self, filename):
        o = super().get_subobject(filename)
        if o is None:
            return self.attachments[filename]
        else:
            return o
class StudentSubmission(ExtensibleContentDirectory):
    def __init__(self, submission_id, date, grade=None, feedback=None):
        super().__init__()
        self.id = submission_id
        self.date = date
        self.grade = grade
        self.feedback = feedback
    def get_time(self):
        return self.date
    def get_files(self):
        pass
    def file_names(self):
        yield 'files'
        if self.grade:
            yield 'grade'
        if self.feedback:
            yield 'feedback'
    def get_subobject(self, name):
        if name == 'files':
            return SubmissionFiles(self)
        elif name == 'grade' and self.grade is not None:
            return DirectContentFile(str(self.grade))
        elif name == 'feedback' and self.feedback is not None:
            return DirectContentFile(self.feedback)
        return super().get_subobject(name)
    def create_file(self, filename):
        if filename == 'grade' or filename == 'feedback':
            return super().create_file(filename)
        else:
            return False
    def file_is_finished(self, filename, data):
        if filename == 'grade':
            self.grade = float(data.decode('utf-8'))
        elif filename == 'feedback':
            self.feedback = data.decode('utf-8')
class SubmissionFiles(DictionaryBasedDirectory):
    def __init__(self, student_submission):
        super().__init__(student_submission.get_files())

class Quiz(Grade):
    def __init__(self, course, name, time, due=None):
        super().__init__(course, name, time, due)

class ContentsOfCourse(ContentDirectory):
    """A directory that contains contents of a course."""
    def __init__(self, parent):
        self.parent = parent
        self.contents = None
    def can_write(self):
        return True
    def get_contents(self):
        """Downloads the contents, if not already done."""
        if self.contents is None:
            self.contents = self.parent.get_content_sections()
        return self.contents
    def file_names(self):
        return [name for name, _ in self.get_contents()]
    def get_subobject(self, filename):
        """Return the file object matching the given filename.
        'filename' is a string.
        Returns a GeneralFile object, or None if the file wasn't found."""
        matched_content = [c for name, c in self.get_contents() if name == filename]
        if len(matched_content) == 1:
            return matched_content[0]
        return None
    def mkdir(self, name):
        """Creates a new directory.
        'name' is a string."""
        self.parent.create_content_section(name)
        self.contents = None
    def rmdir(self, name):
        """Deletes a directory.
        'name' is a string."""
        self.parent.destroy_content_section(name)
        self.contents = None
class UsersOfCourse(ContentDirectory):
    """A directory listing all users of a course."""
    def __init__(self, parent):
        self.parent = parent
    def file_names(self):
        return [x.student_id for x in self.parent.get_users()]
    def get_subobject(self, o):
        return self.parent.get_user_with_id(o)
class GradesOfCourse(ContentDirectory):
    """A directory listing each grade of a course."""
    def __init__(self, parent):
        self.parent = parent
    def file_names(self):
        return [x.name for x in self.parent.get_grades()]
    def get_subobject(self, o):
        return self.parent.get_grade_with_name(o)
class Course(DictionaryBasedDirectory):
    def __init__(self, url, name):
        super().__init__({ 'contents': ContentsOfCourse(self), 'users': UsersOfCourse(self),
            'grades': GradesOfCourse(self)} )
        self.id = url
        self.name = name.strip()
    def get_content_sections(self):
        pass
    def get_users(self):
        pass
    def get_grades(self):
        pass
    def create_content_section(self, name):
        pass
    def destroy_content_section(self, name):
        pass
    def get_user_with_id(self, s_id):
        for u in self.get_users():
            if u.student_id == s_id:
                return u
        return None
    def get_grade_with_name(self, nm):
        for g in self.get_grades():
            if g.name == nm:
                return g
        return None
