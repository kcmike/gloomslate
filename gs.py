"""Gloomslate.

This is the main entry-point, and the executable that should be run.

This module mostly consists of FUSE code, and relies on the Blackboard module to do
the actual scraping."""

import fuse
import sys, stat, errno, os
import traceback
from Blackboard import Blackboard
import Model

fuse.fuse_python_api = 0, 2

log = open('gs.log', 'w', encoding='utf-8')

def break_path(path):
    """Breaks a path up into components.
    'path' is a string, such as 'foo/whatever/b.txt'.
    Returns a list of strings, such as ['foo', 'whatever', 'b.txt'].
    Returns the empty list if given the empty string."""
    if path.startswith('/'):
        path = path[1:]
    comps = []
    i = path.find('/')
    while i != -1:
        comps.append(path[:i])
        path = path[i + 1:]
        i = path.find('/')
    if path:
        comps.append(path)
    return comps

def mkstat(size=0, mode=0, time=0):
    """Creates a Stat object with sensible defaults.
    'size' is a non-negative integer, measured in bytes.
    'mode' is a Unix file mode.
    'time' is a Unix time, i.e., integer seconds since the epoch.
    Returns a fuse.Stat object."""
    if mode & stat.S_IFDIR:
        nlinks = 2
    else:
        nlinks = 1
    return fuse.Stat(st_mode=mode, st_size=size, st_atime=time, st_mtime=time, st_ctime=time,
        st_nlink=nlinks)

class GloomSlate(fuse.Fuse):
    """A class which implements all necessary filesystem operations, via FUSE."""
    class FSRoot(Model.ContentDirectory):
        """A filesystem node representing the root of the filesystem of one course."""
        def __init__(self, course_list):
            self.course_list = course_list
        def file_names(self):
            """(Overriden from Model.ContentDirectory) Return all filenames, as strings"""
            return [c.name for c in self.course_list]
        def get_subobject(self, name):
            """(Overriden from Model.ContentDirectory) Return one file, as an object.
            'name' is a string, indicating the filename of the object.
            Returns a Model.ContentDirectory or Model.ContentFile.
            Returns None if the file doesn't exist"""
            objs = [c for c in self.course_list if c.name == name]
            if len(objs) != 1:
                return None
            return objs[0]
    def __init__(self, base_url, username, password, mountpoint):
        self.parent = Blackboard(base_url, { 'username': username, 'password': password })
        self.parent.login()
        super().__init__()
        self.fuse_args.mountpoint = mountpoint
        self.fuse_args.add('debug')
        self.fuse_args.setmod('foreground')
    def get_object(self, path):
        """(Private) Returns an object for a given path.
        'path' is a string, such as 'a/b/foo.txt'
        Returns a Model.ContentDirectory or Model.ContentFile object.
        Returns None if there's any problem."""
        obj = GloomSlate.FSRoot(self.parent.get_course_list())
        for path_component in break_path(path):
            if not obj.is_directory():
                return None
            obj = obj.get_subobject(path_component)
            if not obj:
                return None
        return obj
    def getattr(self, path):
        """Returns a fuse.Stat for the given path.
        Note this is the most commonly performed operation in a filesystem.
        'path' is a string.
        Returns a fuse.Stat object, or a negative errno integer on error."""
        log.write(f'getattr({path})\n')
        log.flush()
        try:
            obj = self.get_object(path)
            if not obj:
                return -errno.ENOENT
            mode = 0o444 + 0o200 * obj.can_write()
            if obj.is_directory():
                return mkstat(mode=stat.S_IFDIR | 0o111 | mode, time=obj.get_time())
            else:
                return mkstat(mode=stat.S_IFREG | mode, size=obj.get_size(), time=obj.get_time())
        except KeyError as e:
            log.write(f'getattr({path}) => no such file\n')
            log.flush()
        except Exception as e:
            log.write(f'getattr({path}) => {e}\n')
            log.flush()
            traceback.print_exc(file=log)
            log.flush()
    def readdir(self, path, offset):
        """Read the filenames of a directory.
        'path' is a string.
        'offset' is a non-negative integer, and is currently ignored (FIXME).
        DOES NOT RETURN DIRECTLY.
        Instead, we YIELD filenames, as fuse.Direntry objects."""
        log.write(f'readdir({path},{offset})\n')
        log.flush()
        try:
            obj = self.get_object(path)
            if not obj:
                return -errno.ENOENT
            if not obj.is_directory():
                return -errno.ENOTDIR
            yield fuse.Direntry('.')
            yield fuse.Direntry('..')
            for i in obj.file_names():
                yield fuse.Direntry(i)
        except Exception as e:
            log.write(f'readdir({path}) => {e}\n')
            log.flush()
            traceback.print_exc(file=log)
            log.flush()
    def mkdir(self, path, mode):
        """Create a new directory.
        The ContentDirectory.can_write() method is used to determine if this is possible or not.
        'path' is a string.
        'mode' is an integer, representing a Unix mode.
        Returns 0 on success, or a negative errno integer on error."""
        log.write(f'mkdir({path},{mode})\n')
        log.flush()
        path_comps = break_path(path)
        obj = self.get_object('/'.join(path_comps[:-1]))
        if not obj:
            return -errno.ENOENT
        elif not obj.is_directory():
            return -errno.ENOTDIR
        elif not obj.can_write():
            return -errno.EACCES
        if mode & 7 == 0:
            visible = False
        else:
            visible = True
        obj.mkdir(path_comps[-1], visible)
        return 0
    def rmdir(self, path):
        """Removes an empty, existing directory.
        The ContentDirectory.can_write() method is used to determine if this is possible or not.
        'path' is a string.
        Returns 0 on success, or a negative errno integer on error."""
        log.write(f'rmdir({path})\n')
        log.flush()
        path_comps = break_path(path)
        obj = self.get_object('/'.join(path_comps[:-1]))
        if not obj or not obj.is_directory():
            return -errno.ENOENT
        elif not obj.can_write():
            return -errno.EACCES
        obj.rmdir(path_comps[-1])
        return 0
    def open(self, path, flags):
        """Open a regular file.
        We check to ensure the expected opening mode is supported (i.e., if writes are allowed).
        'path' is a string.
        'flags' is a integer, representing a Unix file opening mode.
        Returns 0 on success, or a negative errno integer on error."""
        log.write(f'open({path},{flags})\n')
        log.flush()
        obj = self.get_object(path)
        if not obj:
            return -errno.ENOENT
        if (flags & os.O_RDONLY) and not obj.is_directory():
            return 0
        else:
            return -errno.ENOSYS
    def create(self, path, flags, mode):
        """Create a file.
        'path' is a string.
        'flags' is an integer, I suppose, and is ignored.
        'mode' is an integer, as well, and is ignored equally well.
        Returns 0 on success, or negative errno integer on error."""
        log.write(f'create({path},{flags},{mode})\n')
        log.flush()
        try:
            path_comps = break_path(path)
            obj = self.get_object('/'.join(path_comps[:-1]))
            if obj.is_directory() and obj.can_write() and obj.create_file(path_comps[-1]):
                return 0
            else:
                return -errno.EACCES
        except Exception as e: 
            log.write(f'create({path}) => {e}\n')
            log.flush()
            traceback.print_exc(file=log)
            log.flush()
    def write(self, path, data, offset=0):
        """Write the contents to a file.
        'path' is a string.
        'data' is a list of bytes.
        'offset' is a non-negative integer.
        Returns 0 if okay, or negative errno integer on error."""
        log.write(f'write({path},{len(data)} bytes,{offset})\n')
        log.flush()
        try:
            obj = self.get_object(path)
            if not obj:
                return -errno.ENOENT
            obj.write_bytes(data, offset)
            return len(data)
        except Exception as e: 
            log.write(f'write({path}) => {e}\n')
            log.flush()
            traceback.print_exc(file=log)
            log.flush()
    def truncate(self, path, size):
        log.write(f'truncate({path})\n')
        log.flush()
        return 0
    def flush(self, path):
        """Flush the contents of a file.
        'path' is a string.
        Returns 0 if the file exists, or negative errno integer if it doesn't."""
        log.write(f'flush({path})\n')
        log.flush()
        try:
            obj = self.get_object(path)
            if not obj:
                return -errno.ENOENT
            obj.flush_data()
            return 0
        except Exception as e: 
            log.write(f'flush({path}) => {e}\n')
            log.flush()
            traceback.print_exc(file=log)
            log.flush()
    def release(self, path, something):
        log.write(f'release({path}, {something})\n')
        log.flush()
    def read(self, path, size, offset):
        """Read the contents of a file.
        'path' is a string.
        'size' is a non-negative integer.
        'offset' is a non-negative integer.
        Returns a list of bytes on success, or a negative errno integer on error."""
        log.write(f'read({path},{size},{offset})\n')
        log.flush()
        obj = self.get_object(path)
        if not obj:
            return -errno.ENOENT
        elif obj.is_directory():
            return -errno.EISDIR
        data = obj.get_data()
        if offset >= len(data):
            return b''
        else:
            return data

if len(sys.argv) != 5:
    print('Usage: %s base_url username password mount_point' % sys.argv[0])
    sys.exit(1)

gs = GloomSlate(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
gs.main()
