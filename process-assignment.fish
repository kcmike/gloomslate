#!/usr/bin/fish
set brickwall ~/code/brickwall
if [ (count $argv) -ne 2 ]
	echo "Usage: ./process-assignment.fish <gloomslate-submission-dir> <brickwall-exercise-dir>" >&2
	exit 2
end
for student_no in (ls -1 $argv[1]/)
	echo "Processing student $student_no"
	for submission in (ls -1 $argv[1]/$student_no/)
		echo -n "Student $student_no has submission $submission..."
		set s $argv[1]/$student_no/$submission
		if [ '!' '(' -e $s/feedback -o -e $s/grade ')' ]
			set fs $s/files/*.py
			if count $fs >/dev/null
				cat $s/files/*.py >$student_no.py
				python3 $brickwall/score-single-python-file.py $student_no.py $argv[2]
				cp $student_no.py.feedback $s/feedback
				cp $student_no.py.grade $s/grade
			else
				echo -n "<p>Sorry, you need to submit a .py file</p>" >$s/feedback
				echo -n 0 >$s/grade
			end
			cat $s/grade
		else
			echo already done
		end
	end
end
